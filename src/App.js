import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import ImgList from './Components/ImgList';
import SearchForm from './Components/SearchForm';

const access_key = '74c43e4fe8418fa6cc9e7cfa5756884e9f2c5ddd02ba2c0932b6166d02c46429';
export default class App extends Component {
	constructor() {
		super();
		this.state = {
			imgs: [],
			loadingState: true
		};
	}

	componentDidMount() {
		this.performSearch();
	}

	performSearch = (query = 'new') => {
		axios
			.get(
				`https://api.unsplash.com/search/photos/?page=1&per_page=28&query=${query}&client_id=${access_key}`
			)
			.then(data => {
				this.setState({ imgs: data.data.results, loadingState: false });
				console.log(data.data.results);
			})
			.catch(err => {
				console.log('Error happened during fetching!', err);
			});
	};

	render() {
		return (
			<div>
				<center>
					<nav className="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
						<div>
							<h1 className="navbar-brand">ImageSearch</h1>
							<SearchForm onSearch={this.performSearch} />
						</div>
					</nav>
					<div>
						{this.state.loadingState
							? <p>Loading</p>
							: <ImgList data={this.state.imgs} />}
					</div>
					</center>
			</div>
		);
	}
}
