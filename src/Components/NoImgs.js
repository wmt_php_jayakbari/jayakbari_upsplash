import React from 'react';

const NoImgs =props => (
    <li className='no-imgs'>
        <i>Sorry, No result Found</i>
        <h3>No imges match</h3>
    </li>
);

export default NoImgs;