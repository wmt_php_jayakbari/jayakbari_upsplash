import React from 'react';

const Img = props =>
    <div className="card bg-dark text-white disp">
        <div className="card-body">
            <a href={props.link} target="_blank">
                <h3>{props.id}</h3>
                <img src={props.url} target="_blank" alt="images here.."/>
            </a>
            <p className="card-link">
                Image by<span>&nbsp;</span> 
            <a href={props.user} target="_blank">{props.name}</a><br/>
                <a href={props.link} target="_blank">See it</a>
            </p>
        </div>
    </div>;
export default Img;
