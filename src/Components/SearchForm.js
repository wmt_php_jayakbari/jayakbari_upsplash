import React, { Component } from 'react'

export default class SearchFrom extends Component {
    state = {
        searchText: ''
    };

    onSearchChange = e => {
        this.setState({ searchText: e.target.value });
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.onSearch(this.query.value);
        e.currentTarget.reset();
    };

    render() {
        return (
            <form className="form-inline" onSubmit={this.handleSubmit}>
                <input
                    className="form-control mr-sm-2"
                    type="search"
                    onChange={this.onSearchChange}
                    ref={input => (this.query = input)}
                    name="search"
                    placeholder="Search..."
                />
                <button type="submit" id="submit" className="btn btn-success">
                    <i className="">search</i>
                </button>
            </form>
        );
    }
}
