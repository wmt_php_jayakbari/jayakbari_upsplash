import React from 'react'
function Images(props){
    return(
    <div>
        <h1>{props.id}</h1>
        <img src={props.url} alt="Image" height="auto" width="267px"/>
    </div>
    )}
export default Images