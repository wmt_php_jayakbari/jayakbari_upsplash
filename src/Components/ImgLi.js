import React from 'react';
import Img from './Img';
import NoImgs from './NoImgs';

const ImgLi = props => {
    const data = props;
    let imgs;
   
    if (Object.keys(data).length > 0) {
        imgs = img => 
            <Img 
                url={img.urls.thumb}
                user={img.user.links.html}
                name={img.user.name}
                link={img.links.html}
            />
            ;
    }
    else {
        imgs = <NoImgs />;
    }
    
    return (
        <ul className="img-list">
        {imgs}
        </ul>
    );

};
export default ImgLi;