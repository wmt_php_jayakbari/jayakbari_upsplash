import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import App1 from './App1';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<div><App /></div>,document.getElementById('root'));

serviceWorker.register();
