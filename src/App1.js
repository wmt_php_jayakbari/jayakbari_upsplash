import React, { Component } from 'react';
import axios from 'axios'
import './App.css';
import ImgLi from './Components/ImgLi';

const access_key = '5cd4962541dc8f2d8890aabb3a91fe30bac52fb4e1bde39bcd994088a0a901e8';
class App1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imgs: [],
            loadingState: true
        }
    }
    componentDidMount() {
        axios.get(`https://api.unsplash.com/photos/?client_id=${access_key}`)
            .then(data => {
                this.setState({ imgs: data.data, loadingState: false });
                console.log(data.data);
            })
    }
    render() {

        return (
            <div className="">
                
                <div className="">
					{this.state.loadingState
						? <p>Loading</p>
						: <ImgLi data={this.state.imgs} />}
				</div>
            </div>
        );
    }
}
export default App1;